# SoulSight

## WordPress Plugins 
* Required
    * Advanced Custom Fields
    * Advanced Custom Fields: Options Page
    * Advanced Custom Fields: Repeater Field
    * Duplicate Post
    * Metronet Reorder Posts
    * WP Retina 2x
* Recommended
    * Wordpress Database Backup
    * Regenerate Thumbnails

## Regarding Project Images

### Feature Project Image (on Work page image grid)
* On the 'Work' page projects grid, an image 312px wide by 415px tall is needed.
* Upload it and in that project's admin page, select it as the 'Featured Image'

### Project Images (on project detail pages)
* Size all uploaded images to 2080px x 1300px;
* WordPress will create the following sizes:
    * small  520  X 325  
    * medium 1040 X 650 
    * large  2080 X 1300 - (uses the original uploaded image)
* Based on the currently matched media query (mobile-tablet, desktop, large screen), 
the appropriately sized image will be loaded automatically
* To use a retina image, upload the image with @2x appending to the filename , ex: fortune_1@2x.jpg and
retina.js will automatically find and display it on retina devices.

