<?php 
define('SS_VERSION_NUMBER', '0.0.2');
define('WP_HIDE_DONATION_BUTTONS', true);
// define('DISALLOW_FILE_EDIT', true);


// define('WP_DEFAULT_THEME', 'SoulSight');
require_once dirname(__FILE__). '/inc/bootstrap.php';



/**
 * General configuration for the theme
 */
  add_action( 'after_setup_theme', 'ss_setup' );
 function ss_setup(){

    // ———————————————————————————————-
    // Set image sizes
    // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
    // http://codex.wordpress.org/Function_Reference/add_image_size
    // ———————————————————————————————-
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 312, 415 ); // Default thumbnail size
    add_image_size( 'team-grid', '195', '195', true );
    add_image_size( 'project-grid', '312', '415');
    add_image_size( 'project-featured', '270', '270', true);

    // add_image_size( 'page-splash', '520',  '325');

    // small  520  X 325  - project-img-sm
    // medium 1040 X 650  - project-img-md
    // large  2080 X 1300 - project-img-lg
    add_image_size( 'project-img-sm', '520',  '325');
    add_image_size( 'project-img-md', '1040', '650');
    add_image_size( 'project-img-lg', '2080', '1300');
 }


/**
 * Enqueue theme stylesheet files
 *
 * http://codex.wordpress.org/Function_Reference/wp_enqueue_style
 */
add_action( 'wp_enqueue_scripts', 'ss_enqueue_styles' );
function ss_enqueue_styles() {
    wp_enqueue_style( 'screen', get_template_directory_uri() . '/assets/css/screen.css', array(), SS_VERSION_NUMBER, 'all' );
}

/**
 * Enqueue theme Javascript files
 *
 * http://codex.wordpress.org/Function_Reference/wp_enqueue_script
 */
add_action( 'wp_enqueue_scripts', 'ss_enqueue_scripts' );
function ss_enqueue_scripts() {
    wp_enqueue_script( 'modernizr', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js', array(), SS_VERSION_NUMBER);
    wp_enqueue_script( 'enquire', get_template_directory_uri() . '/assets/js/libs/enquire.js', array(), SS_VERSION_NUMBER);
    //wp_enqueue_script( 'retina', get_template_directory_uri() . '/assets/js/libs/retina.min.js', array(), SS_VERSION_NUMBER);
    wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/assets/js/libs/jquery.easing.js', array('jquery'), SS_VERSION_NUMBER);
    wp_enqueue_script( 'jquery-waypoints', get_template_directory_uri() . '/assets/js/libs/waypoints.min.js', array('jquery'), SS_VERSION_NUMBER);
    wp_enqueue_script( 'jquery-waypoints-sticky', get_template_directory_uri() . '/assets/js/libs/waypoints-sticky.min.js', array('jquery-waypoints'), SS_VERSION_NUMBER);
    wp_enqueue_script( 'jquery-tiptop', get_template_directory_uri() . '/assets/js/libs/jquery.tiptop.js', array('jquery'), SS_VERSION_NUMBER);
    wp_enqueue_script( 'picturefill', get_template_directory_uri() . '/assets/js/libs/picturefill.js', array(), SS_VERSION_NUMBER);
    wp_enqueue_script( 'script', get_template_directory_uri() . '/assets/js/script.js', array('jquery', 'jquery-waypoints', 'jquery-easing', 'picturefill'), SS_VERSION_NUMBER,  true );
}


/**
 *  Register Navigation Menus
 *
 */
add_action( 'init', 'navigation_menus' );
function navigation_menus() {

    $locations = array(
        'header_menu' => __( 'Header Menu', 'soul-sight' ),
    );
    register_nav_menus( $locations );
}


/*
*  Create a simple sub options page called 'Footer'
*/
if( function_exists('acf_add_options_sub_page') )
{
    // acf_add_options_sub_page( 'General' );
    acf_add_options_sub_page( 'Social Media' );
}


function truncated_title($limit = 49){
    $title = substr(get_the_title(), 0, $limit);
    if (strlen($title) > $limit) {
        $title .=  " ...";
    }
    return $title;
}
