<?php /*Global Footer*/ ?>
    <footer class="site-footer">
      <?php get_template_part( 'partials/_site-footer' ); ?>
    </footer>

    <?php wp_footer();?> 
    <?php get_template_part('partials/_analytics'); ?>
     
    </div><!-- .site-wrapper -->
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/libs/retina.min.js?ver=0.0.1"></script>

<!--[if lte IE 9]><!-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/ie.js"></script>
<!--<![endif]-->

</body>
</html>