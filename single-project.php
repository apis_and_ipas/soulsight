<?php 
/**
*  single-project.php
*/
get_header(); ?>
<!-- <section class="splash">

  <?php if(get_field('splash_image')):?>
      <?php // show the splash image if it exists?>
      <img src="<?php the_field('splash_image', get_the_ID()); ?>" >
  <?php else: ?>
      <?php //otherwise show the splash image from the home page ?>
      <img src="<?php the_field('splash_image', 8); ?>" >
  <?php endif;?>
  
</section> -->



<section class="project-images" style="max-width: 2440px; margin: auto;">

      <?php $images = get_field('images', get_the_ID()); 

        // small  520  X 325  - project-img-sm
        // medium 1040 X 650  - project-img-md
        // large  2080 X 1300 - full
        if ($images):
            $i = 0;
            foreach ($images as $image_id):
              $image_obj =  wp_get_attachment_image_src( $image_id['image'], 'full');
              $full_path = $image_obj[0];
              $img_path = explode('/', $image_obj[0]);
              $file_name = $img_path[count($img_path) - 1];

              $fn_parts = explode('.', $file_name);
              $sm_image_name = "$fn_parts[0]-520x325.$fn_parts[1]";
              $md_image_name = "$fn_parts[0]-1040x650.$fn_parts[1]";
              $lg_image = $image_obj[0];
              
              $sm_image = str_replace($file_name, $sm_image_name, $full_path);
              $md_image = str_replace($file_name, $md_image_name, $full_path);

            ?>
            <span data-picture>
              <span data-src="<?php echo $sm_image; ?>"></span>
              <span data-src="<?php echo $md_image; ?>" data-media="(min-width: 1030px)"></span>
              <span data-src="<?php echo $lg_image; ?>" data-media="(min-width: 1240px)"></span>
             
              <!-- Fallback content for non-JS browsers. Same img src as the initial, unqualified source element. -->
              <noscript>
                  <img src="<?php  echo $lg_image; ?>"/>
              </noscript>
            </span>
            

            <?php 
              // Only show project Detail after the first Image.
              if ($i === 0): ?>
          
              <section class="project-detail" style="">
                <div class="wrapper">
                  <h1 class="project-name"><?php the_title(); ?></h1>
                  
                  <div class="project-categories">
                    <?php comma_seperated_terms('project-type'); ?>
                  </div>

                  <?php if ( get_field('project_description', get_the_ID()) ): ?>
                    <div class="project-description">
                         <?php the_field('project_description', get_the_ID()); ?> 
                    </div>
                  <?php endif; ?>


                </div><!-- wrapper -->
              </section>

            <?php endif; $i++;?>
  
        <?php endforeach; endif; ?>

</section>

<section class="know-more">
  <div><strong>Want to know more?</strong></div>
  <div><a href="mailto:info@soulsight.com">Start A conversation with us at info@soulsight.com</a></div>
  <div><a href="mailto:info@soulsight.com"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/env-icon.png" alt=""></a></div>
</section>

<script>
  (function($){ 

    /**
    *    Dirty Hack to fix Custom Post Type / Current menu-item issues
    */
    if ( $('body').hasClass('single-project') ) {
       $('.current_page_parent').removeClass('current_page_parent');
       $('.menu-item-23').addClass('current-menu-item');
    }

  })(jQuery);
</script>
<?php get_footer(); ?>