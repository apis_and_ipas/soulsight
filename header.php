<!DOCTYPE html>
<!--[if IEMobile 7 ]><html class="no-js iem7""><![endif]-->
<!--[if lt IE 7 ]><html class="no-js ie6 ie-lt-7 ie-lt-8 ie-lt-9"><![endif]-->
<!--[if IE 7 ]><html class="no-js ie7 ie-lt-8 ie-lt-9"><![endif]-->
<!--[if IE 8 ]><html class="no-js ie8 ie-lt-9"><![endif]-->
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!-->
<html class="no-js" >
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php add_fb_og_meta(); ?>

    <title>soulsight&reg; | Chicago Branding Agency - Packaging Design, Brand Identity & Brand Strategy Consultants</title>

    <meta name="description" content="Soulsight - Global package design, corporate and brand identity consultants.">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    
    <!--[if lte IE 9]><!-->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/libs/matchMedia.polyfill.js"></script>
    <!--<![endif]-->

    <!--[if lte IE 8]><!-->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/libs/respond.js"></script>
    <!--<![endif]-->

    <?php wp_head();?>

    <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/favicon.ico" type="image/x-icon">

</head>
<body <?php body_class(); ?>>

  <div class="site-wrapper">  

      <?php if (is_page('home') || is_singular('project') && get_field('splash_image')):?>
          <section class="splash" id="front-splash" style="background-image: url(<?php echo esc_attr( get_field('splash_image') ); ?>);">
              <div class="splash-text">
                <div class="wrapper">
                  <h1><?php echo get_field('splash_text' );?></h1>
                  <a href="#" class="splash-link" data-scroll-to=".pullquote-1" ><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/down-arrow-splash.png" alt=""></a>
                </div>  
              </div>
          </section>
      <?php endif; ?> 

      <header class="site-header">
        <?php get_template_part( 'partials/_site-header' ); ?>
      </header>