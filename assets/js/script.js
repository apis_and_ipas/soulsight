
;(function($){
      var isMobile;
      // Adds class to body to indicate Current Media Query
      enquire
      .register("screen and (max-width: 1030px)", {
          match: function(){
            // console.log('match mobile');
            $('body').addClass('mobile-device');
            isMobile = true;
          },
          unmatch: function(){
            // console.log('unmatch mobile');
            $('body').removeClass('mobile-device');
  

          }
      })
      .register("screen and (min-width:1030px)", {
          match: function(){
            // console.log('match desktop');
            $('body').addClass('desktop');
            isMobile = false;
          },
          unmatch: function(){
            // console.log('unmatch desktop');
            $('body').removeClass('desktop');

          }
      });



      $(function(){
        
          // Cached selectors
          var $siteHeader = $('.site-header'),
              $menuTrigger = $('.mobile-nav-toggle'),
              $blogFilterTrigger = $('.list-icon, .filter-close'),
              $blogSearchTrigger = $('.search-icon, .search-close');

          // Toggle Mobile Nav state
          $menuTrigger.click(function(){
              $('body').toggleClass('mobile-nav-active mobile-nav-inactive');
          });

          // Toggle for blog Filter-by-category list
          $blogFilterTrigger.click(function(e){
              e.preventDefault();
              $('.filter-box').slideToggle();
              $('body').toggleClass('blog-filter-active blog-filter-inactive');
              $('.actions').fadeToggle();
          });

          // Toggle for Blog search
          $blogSearchTrigger.click(function(e){
              e.preventDefault();
              $('.search-box').slideToggle();
              $('body').toggleClass('blog-search-active blog-search-inactive');
              $('.actions').fadeToggle();
          });

          // Sticky Global Header
          $siteHeader.waypoint('sticky');
          
          // Services Page
          if ( $('body').hasClass('page-services') ) {

              $('.sticky-menu').waypoint(function(direction){
                  var $this = $(this);

                  if (direction === 'down') {
                    $this.addClass('fixed-menu');
                  } else if (direction === 'up') {
                    $this.removeClass('fixed-menu');
                  }
              });


              // Prevent sticky menu from crashing into the footer by hiding it 
              // when the last content block hits the top of the page
              $('#additional').waypoint(function(direction){
                  var $this = $(this);

                  if (direction === 'down') {
                      // console.log('end-of-services waypoint: down');
                      $('.sticky-menu').fadeOut();
                      $('.end-msg').fadeIn();
                  } else if (direction === 'up') {
                      // console.log('end-of-services waypoint: up');
                      !isMobile &&  $('.sticky-menu').fadeIn();
                      $('.end-msg').fadeOut();
                  }
              });


              // As we pass each service as scrolling
              $('.service').waypoint(function(direction){
                  // Grab the services #
                  var sel = $(this).attr('id');

                  // Removes the previously active service's class from the 
                  // Services wrapper-container (this changes the bg color)
                  $('#services').attr('class', '').addClass(sel);

                  // Clear active classes on all sticky menu links
                  $('.sticky-menu a').attr('class', '');
                  // Add active class to the currently active service area.
                  $('.sticky-menu a[href=#' + sel + ']').addClass('active');
                  
              }, { offset: 210 });

          }


          // Easy animated scroll via data-scroll-to attribute, pass selector as value
          //  <a href="#" data-scroll-to="#awesomeness">Check It Out!</a>
          $('[data-scroll-to]').click(function(e){
              e.preventDefault();
              var $target = $( $(this).data('scroll-to') );

              $('html,body').animate({
                  scrollTop: $target.offset().top - 60
              }, 1000, 'easeInOutQuint');
          });


          // Fire off map markers plugin on About page
          if ( $('body').hasClass('page-about') ) {
            $('.map-marker').tiptop({ content: 'location' });
            // $('.mobile-device .tiptop').css('display', 'none');
          }

          // Twitter Share
          // Hipster Hack #4 - Guard Operator && http://berzniz.com/post/68001735765/javascript-hacks-for-hipsters
          $(".twitter-share").click(function(e) {
            var win = window.open($(this).attr("href"), "Share on Twitter", "height=400,width=530");
            window.focus && win.focus(); 
            win && e.preventDefault();
          });

          //Facebook Share
          $(".facebook-share").click(function(e) {
            var win = window.open($(this).attr("href"), "Share on Facebook", "height=400,width=530");
            window.focus && win.focus();
            win && e.preventDefault();
          });


      });//dom ready
      
   
})(jQuery);