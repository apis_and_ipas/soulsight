/*!
 * Sheetrock Tabs Plugin
 * Original author: Bryan Paronto <bparonto@gmail.com>
 * Licensed under the MIT license
 */

;(function($, window, document, undefined) {

  $.fn.tiptop = function(options) {

    if (!this.length) { return this; }

    // default options
    $.fn.tiptop.defaults = {
      debug: false,
      content: 'content',
      css: {
        'position' : 'absolute',
        'z-index' : '1000',
        'margin-left' : '-35px',
        'margin-top' : '-50px'
      }
    };

    var opts = $.extend(true, {}, $.fn.tiptop.defaults, options);

    this.each(function() {

      
      $(this).hover(function(){

          if (opts.debug) { console.log('hoverIn'); }

          var $this   = $(this),
              content = $this.data(opts.content),
              left    = $this.css('left'),
              top     = $this.css('top'),
              $el = $('<div class="tiptop">');

          
          if( !$('body').hasClass('mobile-device') ){ // do not fire on mobile-devices

              //Apply default styles and dynamically grabbed x & y position
              $el.css(opts.css).css({
                'left': left,
                'top' : top
              });

        
              // Append the element
              $this.after($el);

              // Insert the content
              $el.text(content);

          }
            

        }, function(){

          if (opts.debug){ console.log('hoverOut'); }

          // Remove the element
          $('.tiptop').remove();

        }
      );
    });

    
    return this;
  };

})(jQuery, window, document);