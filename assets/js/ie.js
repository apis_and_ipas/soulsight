;(function($){

    // DOM Ready
    $(function(){

        // Fixes 4-across Services on home page
        $('.ie-lt-9 .home .service:last').css({
            'margin-right':'0'
        });


        polyfillSearchPlaceholder();


    });//dom ready


    // Functions
    function polyfillSearchPlaceholder(){
        if (!Modernizr.input.placeholder) {
            // Polyfill for 'Search' Placeholder in search.
            $('#search-form input[type="text"]')
                .attr('value', 'Search')
                .focus(function(){
                    $(this).attr('value', '');
                })
                .blur(function(){
                    $(this).attr('value', 'Search');
                });
        }
    }// end polyfillSearchPlaceholder
    
})(jQuery);