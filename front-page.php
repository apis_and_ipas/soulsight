<?php 
/**
*  front-page.php
*/
get_header(); ?>

<section class="pullquote-1">
  <div class="wrapper prose">
    <?php the_field('pullquote_one'); ?>
  </div>
</section>

<section class="services">
  <header class="module-header">
      <div class="wraper">Our Services</div>
  </header>
  <div class="module-body">
      <div class="wrapper">
          <div class="service strategy" style="background-image: url(<?php echo esc_attr( get_field( 'icon_1' ) ) ?>);">
            <div class="header"><?php echo esc_html( get_field( 'service_name_1' ) )?></div>
            <p><?php echo esc_html( get_field( 'description_1' ) )?></p>
          </div>
          <div class="service branding" style="background-image: url(<?php echo esc_attr( get_field( 'icon_2' ) ) ?>);"> 
            <div class="header"><?php echo esc_html( get_field( 'service_name_2') )?></div>
            <p><?php echo esc_html( get_field( 'description_2' ) )?></p>
          </div>
          <div class="service packaging" style="background-image: url(<?php echo esc_attr( get_field( 'icon_3' ) ) ?>);">
            <div class="header"><?php echo esc_html( get_field( 'service_name_3') )?></div>
            <p><?php echo esc_html( get_field( 'description_3' ) )?></p>
          </div>
          <div class="service structural" style="background-image: url(<?php echo esc_attr( get_field(  'icon_4' ) ) ?>);">
             <div class="header"><?php echo esc_html( get_field( 'service_name_4' ) )?></div>
             <p><?php echo esc_html( get_field( 'description_4' ) ) ?></p>
          </div>
      </div>
  </div>
  <footer class="module-footer">
    Learn More About Our Capabilities
    <a href="/services"><div class="arrow right"></div></a>
  </footer>
</section>

<section class="pullquote-2" style="background-image: url(<?php the_field('pullquote_two_background'); ?>)">
  <div class="wrapper prose">
    <b class="open"></b>
    <?php echo get_field( 'pullquote_two' ); ?>
    <b class="close"></b>
  </div>
</section>

<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/hr.png" class="hr">

<section class="featured-projects">
  <header class="module-header">
      <div class="wraper">Featured Projects</div>
  </header>
  <div class="module-body">
      <div class="wrapper">
          <?php get_template_part( 'partials/_featured', 'projects' ); ?>
      </div>
  </div>
  <footer class="module-footer">
    Take A Look At All Of Our Client Work
    <a href="/work"><div class="arrow right"></div></a>
  </footer>
</section>

<!-- <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/hr.png" class="hr"> -->
<div class="img-above-footer" style="background-image: url(<?php the_field('image_above_footer')?>);"></div>
<script>
  
</script>
<?php get_footer(); ?>