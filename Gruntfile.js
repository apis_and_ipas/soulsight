module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        rsync: grunt.file.readJSON('rsync-config.json'),
        sass: {
            dist: {
                options: {
                    style: 'compressed'
                },
                files: {
                    './assets/css/screen.css': './src/scss/screen.scss',
                    './assets/css/admin-styles.css': './src/scss/admin-styles.scss'
                }
            },
            dev: {
                options: {
                    style: 'nested'
                },
                files: {
                    './assets/css/screen.css': './src/scss/screen.scss',
                    './assets/css/admin-styles.css': './src/scss/admin-styles.scss'
                }
            }
        },
        jshint: {
            options: {
                browser: true,
                '-W030': true,
                globals: {
                    jQuery: true
                },
            },
            all: ['./src/js/**/*.js', 'Gruntfile.js']
        },
        watch: {
            options: {
                livereload: true,
            },
            sass: {
                files: ['**/*.scss'],
                tasks: ['sass:dev']
            },
            scripts: {
                files: ['./assets/js/**/*.js', 'Gruntfile.js'],
                tasks: ['jshint']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-rsync');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('build', ['scss:dist']);
    grunt.registerTask('deploy-staging', ['rsync:stage','rsync:stage_plugins','rsync:stage_uploads']);
    grunt.registerTask('deploy-production', ['rsync:prod','rsync:prod_plugins','rsync:prod_uploads']);


};