<?php 
/**
*  Services
*/
get_header(); ?>

<div id="top-of-services"></div>

<?php if ( get_field('splash_image' ) ): ?>
<section class="splash"><img src="<?php echo esc_attr( get_field('splash_image' ) ); ?>" ></section>
<?php endif; ?>

<section id="services">
  <div class="wrapper">
    <div class="grid-3">
      &nbsp;
      <nav class="sticky-menu">
        <ul>
          <li><a href="#strategy">Strategy</a></li>
          <li><a href="#branding">Branding</a></li>
          <li><a href="#packaging">Packaging</a></li>
          <li><a href="#structural">Structural</a></li>
          <li><a href="#additional">Additional</a></li>
        </ul>
      </nav>
    </div>
    <div class="content grid-9">

        

        <div id="strategy" class="service" style="background-image: url(<?php the_field('service_icon_1'); ?>);">

            <?php if ( get_field('service_name_1') ): ?>
                <h2><?php the_field('service_name_1'); ?></h2>
            <?php endif; ?>

            <?php if ( get_field('service_desc_1') ): ?>
                <?php the_field('service_desc_1'); ?>
            <?php endif; ?>

        </div><!-- #strategy -->



        <div id="branding" class="service" style="background-image: url(<?php the_field('service_icon_2'); ?>);">

            <?php if ( get_field('service_name_2') ): ?>
                <h2><?php the_field('service_name_2'); ?></h2> 
            <?php endif; ?>
            
            <?php if ( get_field('service_description_2') ): ?>
                <?php the_field('service_description_2'); ?>
            <?php endif; ?>

        </div><!-- #branding -->




        <div id="packaging" class="service" style="background-image: url(<?php the_field('service_icon_3'); ?>);">

            <?php if ( get_field('service_name_3') ): ?>
                <h2><?php the_field('service_name_3'); ?></h2> 
            <?php endif; ?>
            
            <?php if ( get_field('service_description_3') ): ?>
                <?php the_field('service_description_3'); ?>
            <?php endif; ?>

        </div><!-- #packaging -->




         <div id="structural" class="service" style="background-image: url(<?php the_field('service_icon_4'); ?>);">

            <?php if ( get_field('service_name_4') ): ?>
                <h2><?php the_field('service_name_4'); ?></h2>
            <?php endif; ?>
            
            <?php if ( get_field('service_description_4') ): ?>
                <?php the_field('service_description_4'); ?>
            <?php endif; ?>

        </div><!-- #structural -->



        <div id="additional" class="service" style="background-image: url(<?php the_field('service_icon_5'); ?>);">

            <?php if ( get_field('service_name_5') ): ?>
                <h2><?php the_field('service_name_5'); ?></h2> 
            <?php endif; ?>

            <?php if ( get_field('service_description_5') ): ?>
                <?php the_field('service_description_5'); ?>
            <?php endif; ?>


          <br>
          <br>
          <br>
          <br>
          <br>

          
          <a href="#top-of-services"  style="display: none;" class="end-msg">Back to Top&nbsp;&nbsp;&#9650;</a>
        </div><!-- #additional -->
      
        
        <div id="the-end"></div>

        
       
        

    </div><!-- .content -->
    
  </div><!-- .wrapper -->
</section>

<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/hr.png" class="hr">

<?php get_footer(); ?>