<?php 
/**
*  Index.php
*/
get_header(); ?>


<section class="splash"><img src="<?php the_field('splash_image', 10); ?>" ></section>


<section class="search-categories">
  <div class="wrapper">

    <div class="search-box">
      <div class="search-close">&times;</div>
      <?php get_search_form(); ?>
    </div>

    <div class="filter-box">
      <div class="filter-close">&times;</div>
      <!-- Filters -->    

      <?php 
        $list_count = 0;
        $args = array(
          'orderby' => 'name',
          'order'   => 'ASC'//,
          // 'hide_empty' => 0
        );

        $categories = get_categories( $args ); 
        echo "<ul class='list-col'>";
          foreach ($categories as $category):
            echo '<li><a href="' . esc_attr( get_category_link( $category->term_id ) ). '" title="' . sprintf( __( "View all posts in %s" ), $category->name ) . '" ' . '>' . $category->name.'</a></li>';
            $list_count++;
            if ($list_count % 7 == 0) echo "</ul><ul class='list-col'>";
          endforeach;
        echo "</ul>";
      ?> 
      <!-- <ul>
        <?php //wp_list_categories('orderby=name&hide_empty=0&title_li='); ?>
      </ul> -->
      
    </div>

    <div class="actions">
      <a href="#" class="list-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/list-icon.png" alt=""></a>
      <a href="#" class="search-icon"><img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/search-icon.png" alt=""></a>
    </div>

  </div>
</section>

<section class="blog-index">
  
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

       <div class="post-listing" id="<?php echo the_slug(); ?>">
          <div class="wrapper">
              <a href="<?php the_permalink();?>">
                <div class="post-date"><?php the_time('F j, Y'); ?></div>  
                <div class="post-title"><?php echo truncated_title(); ?></div> 
              </a><!-- permalink -->
          </div><!-- .wrapper -->
       </div><!-- .post-listing --> 
      
    <?php endwhile; ?> 

      <div class="pagination">
        <div class="wrapper">
          <div class="left-links"><?php previous_posts_link('<span class="left-arrow"></span>', 0); ?></div> 
          <div class="right-links"><?php next_posts_link('<span class="left-arrow"></span>', 0); ?></div> 
        </div>
      </div>

    <?php else: ?>

      <section class="nothing-found">
        <div class="wrapper">

          <?php if ( is_category() ): ?>
            <div class="msg">Sorry - No Posts are available for this category.</div>
          <?php endif; ?>

          <?php if ( is_search() ): ?>
            <div class="msg">Sorry - No results were returned. Try Seaching again?</div> 
            <div class="search-box" style="display:block;">
              <?php get_search_form(); ?>
            </div>
          <?php endif; ?>

          <?php if ( is_404() ):?>
            <div class="msg">404! We're sorry. We can't seem to find what you were looking for. Try Searching?</div>
            <div class="search-box" style="display:block;">
              <?php get_search_form(); ?>
            </div>
          <?php endif; ?>

        </div>
      </section>
      
    <?php endif; ?>


</section>


<?php get_footer(); ?>