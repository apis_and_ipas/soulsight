<?php 
/**
*  Work
*/
get_header(); ?>

<?php if ( get_field('splash_image' ) ): ?>
<section class="splash"><img src="<?php  echo esc_attr( get_field('splash_image' ) ); ?>" ></section>
<?php endif; ?>

<section class="project-grid">
  <div class="wrapper">
    <?php get_template_part('partials/_project-grid')?>
  </div>
</section>


<section class="clients">
  <div class="wrapper">
    <?php get_template_part('partials/_client-grid')?>
  </div>
</section>

<section class="services">
  <a href="/services">
    Check out all the services we offer
    <div class="arrow right"></div>
  </a>
</section>

<?php get_footer(); ?>