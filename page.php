<?php 
/**
*  page.php
*/
get_header(); ?>


<section class="splash"><img src="<?php echo esc_attr( get_field('splash_image', 10 ) ); ?>" ></section>


<section class="content">
  
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

       <div class="post-listing" id="<?php echo the_slug(); ?>">
          <div class="wrapper">
              
              <h1 class="page-title"><?php echo truncated_title(); ?></h1> 
              
              <div class="page-content">
                <?php the_content(); ?>
              </div>

          </div><!-- .wrapper -->
       </div><!-- .post-listing --> 
      
    <?php endwhile; endif; ?>


</section>


<?php get_footer(); ?>