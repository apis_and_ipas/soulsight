<?php 
/**
*  About Page
*/
get_header(); ?>
<?php if ( get_field('splash_image' ) ): ?>
<section class="splash"><img src="<?php echo esc_attr( get_field('splash_image' ) ); ?>" ></section>
<?php endif; ?>

<section class="we-are-soulsight">
  <div class="wrapper prose">
      <h1>We Are Soulsight</h1>
      <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
          <?php the_content(); ?>
      <?php endwhile; endif; ?>
  </div>
</section>

<section class="the-team">
  <header class="module-header">The Team</header>
  <div class="module-body"> 
    <div class="wrapper">
       
        <?php get_template_part( 'partials/_team-loop'); ?>

    </div> 
  </div>
  <footer class="module-footer">
    We are always interested in new talent
    <a href="#join-the-team" data-scroll-to="#join-the-team"><div class="arrow down"></div></a>
  </footer>
</section>

<section class="join-the-team" id="join-the-team">
  <div class="wrapper prose">
      <div class="copy">
        <h2>Want to Join Our Team? Let's Chat.</h2>
        <?php echo get_field('join_the_team_text');?>
      </div>
      <div class="listings">
        <?php 
        $positions = get_field('job_postings'); 
        $i = 0;
        if ($positions): ?>
          Currently we are on the hunt for:
          <ul>
        <?php 
        foreach($positions as $position):
          $id = $position->ID; 
          $i++;
        ?>
            <li><a href="<?php echo get_permalink( $id );?>"><?php echo get_the_title( $id );?></a></li>
           <?php if ($i % 4 == 0):?>
           </ul><ul>
           <?php endif;?> 
        <?php endforeach; ?>
        </ul>

       <?php else: ?>
          Sorry, no positions currently available!
       <?php endif; ?>
        
      </div>  
  </div>
</section>

<section class="find-us">
  <header class="module-header">
    Our Offices
  </header>
  <div class="module-body world-map-wrap">
    <div class="wrapper prose">

      <div class="world-map">
        <div class="map-marker teal" id="north-america" data-location="Chicago, Illinois"></div>
        <div class="map-marker red" id="europe" data-location="Madrid, Spain"></div>
        <div class="map-marker red" id="latin-america" data-location="Mexico City, Mexico"></div>
        <div class="map-marker green" id="asia" data-location="Seoul, Korea"></div>
        <div class="map-marker teal" id="africa" data-location="Ikeja, Lagos, Nigeria"></div>
        <div class="map-marker green" id="south-america" data-location="Sao Paulo, Brazil"></div>
      </div>
      
      <div class="copy">
        <h2>You can find us all over the world</h2>
        <?php echo esc_html( the_field( 'global_alliance_text' ) ); ?>
      </div>

    </div>
  </div>
</section>
<img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/hr.png" class="hr">
<?php get_footer(); ?>