<?php
require_once 'helpers.php';


/**
 * Load 'Features'
 */
add_action( 'after_setup_theme', 'ss_load_features' );
function ss_load_features(){
  $path = __DIR__ . '/features/';

  $features = array(
    'customize-admin',
    'fb_js_sdk',
    'fb_og_meta',
    'wp-head-cleanup',
    'slug-in-body-class'
  );

  foreach ( $features as $feature ) {
    require $path.$feature.'.php';
  }
}

add_action( 'after_setup_theme', 'ss_load_cpt' );
function ss_load_cpt(){
  $path = __DIR__ . '/custom-post-types/';

  $types = array(
    'client',
    'project',
    'team-member'
  );

  foreach ( $types as $type ) {
    require $path.$type.'.php';
  }
}




function the_slug(){
  global $post;
  return $post->post_name;
}