<?php

  register_post_type('team-member', 
    array(
        'labels'        => ss_auto_labels('Team Member'),
        'description'   => '',
        'public'        => true,
        'has_archive'   => false,
        'menu_position' => '5',
        'menu_icon' => '',
        'supports' => array( 'title', 'thumbnail')//,
      )
  ); 