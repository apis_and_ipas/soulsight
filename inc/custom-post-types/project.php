<?php
  // register_post_type('faq', 
  //   array(
  //       'labels'        => ss_auto_labels('FAQ'),
  //       'description'   => '',
  //       'public'        => true,
  //       'has_archive'   => false,
  //       'menu_position' => '5',
  //    // 'menu_icon' => '',
  //       'supports' => array( 'title', 'editor'),
  //     )
  // ); 

  register_post_type('project', 
    array(
        'labels'        => ss_auto_labels('Project'),
        'description'   => '',
        'public'        => true,
        'has_archive'   => true,
        'menu_position' => '5',
        'menu_icon' => '',
        'supports' => array( 'title', 'thumbnail' )
      )
  ); 

register_taxonomy(
  'project-type',
  'project',
  array(
    'label' => __( 'Project Type' ),
    'rewrite' => array( 'slug' => 'project-type' ),
    'hierarchical' => false,
  )
);


