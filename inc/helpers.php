<?php

/**
*   General Labels for Custom Post types.
*
*   @param  string $singular 
*   @param  string $plural (optional)
*   @return array
*
*/
function ss_auto_labels( $singular, $plural = '' ) {
    if ( empty( $plural ) )
        $plural = $singular . 's';
 
    return array(
        'name' => $plural,
        'singular_name' => $singular,
        'search_items' => 'Search ' . $plural,
        'all_items' => $plural,
        'edit_item' => 'Edit ' . $plural,
        'add_new_item' => 'Add New ' . $plural,
        'menu_name' => $plural,
        'new_item' => 'New ' . $singular,
        'view_item' => 'View ' . $plural,
        'not_found' => 'No ' . $plural . ' found',
        'not_found_in_trash' => 'No ' . $plural . ' found in Trash',
        'parent_item_colon' => '',
    );
}

/**
*   Returns a copyright year range for use in Copyright info
* 
*   @param string $start_copyright_year
*   @return string
*
*/
function ss_copyright_range($start_copyright_year = "2013") {
    $current_year = (int) date('Y');
    
    // Don't display the range if the end year is the same as the start year, instead just display the year
    if( $start_copyright_year >= $current_year ) {
        return $current_year;
    } else {
        return $start_copyright_year . ' - ' . $current_year;
    }
}

function comma_seperated_terms($taxonomy){
    global $post;

    $categories = wp_get_object_terms( $post->ID, $taxonomy);
    $count = count($categories); 
    $i = $count;
        
    foreach ($categories as $category) {
        $i--; 
        if ($count >= 1){
            $sep = ($i == 0) ? " " : ", ";
        } else {
            $sep = " ";
        }
    
        echo $category->name . $sep;
    }
}