<?php


function add_fb_og_meta(){
  global $post;
  if ($post && $post->ID){
     $permalink = get_permalink( $post->ID );
     $title = $post->post_title;
  } else {
    $permalink = site_url();
    $title = "Soul Sight";
  }
  ?>
  <!-- Facebook OpenGraph Metadata -->
  <meta property="og:url" content="<?php echo $permalink ?>" />
  <meta property="og:site_name" content="<?php echo bloginfo( 'name' ); ?>" />
  <meta property="og:type" content="website" />
  <meta property="og:title" content="<?php echo $title;  ?>" />
  <meta property="og:image" content="<?php echo get_stylesheet_directory_uri() ?>/assets/img/meta-logo.png" />
  <meta property="og:description" content="<?php echo bloginfo( 'description' ); ?>" />
  <meta property="fb:app_id" content="282058518608469" />

  <?php
}