<?php 

add_filter( 'body_class', 'add_slug_body_class' );
function add_slug_body_class( $classes ) {
    global $post;
    
    if ( isset( $post ) ) {
        $classes[] = $post->post_type . '-' . $post->post_name;
        $classes[] = 'mobile-nav-inactive';
        $classes[] = 'blog-filter-inactive';
        $classes[] = 'blog-search-inactive';
    }
    
    return $classes;
}