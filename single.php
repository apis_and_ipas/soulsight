<?php 
/**
*  Index.php
*/
get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<section class="splash">
  <div class="wrapper">
    <h1><?php the_title(); ?></h1>
    <h3><?php the_date(); ?></h3>
  </div>
</section>

<section class="content">
  <div class="wrapper prose">
    <?php
      $post_title = urlencode(get_the_title());
      $post_url = urlencode(get_permalink());

      $clean_url = str_replace("http%3A%2F%2F", "", $post_url);

      $fb_path = "http://www.facebook.com/sharer/sharer.php?u=" . $clean_url;
      $twt_path = "http://twitter.com/share/?text=" . $post_title . ' ' ;
      ?>

    <div class="share">
      Share
       <a href="<?php echo $fb_path; ?>" class="facebook-share social fb" title="Facebook"></a> 
       <a href="<?php echo $twt_path;?>" class="twitter-share social twt" title="Twitter"></a>
    </div>


    <div class="copy">
      <?php the_content();?>
    </div> 
  </div>
  <footer class="post-footer">
    <a href="/blog#<?php echo the_slug();?>">
    Back To Our Other Posts
    <div class="arrow left"></div>
    </a>
  </footer>
</section>
<?php endwhile; endif; ?>

<?php get_footer(); ?>