<?php 
/**
* Don't include analytics in dev or staging env
*
*/

  if (!strpos($_SERVER['HTTP_HOST'], '.dev') || !strpos($_SERVER['HTTP_HOST'], 'doejoapp.com')): ?>
        
        <!-- start analytics -->
        <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
        </script>
        <script type="text/javascript">
        try {
        var pageTracker = _gat._getTracker("UA-478094-16");
        pageTracker._trackPageview();
        } catch(err) {}
        </script>
        <!-- end analytics -->

        
<?php endif; ?>