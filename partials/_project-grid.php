<?php

$projects = new WP_Query(array(
  'post_type'   => 'project',
  'post_status' => 'publish',
  'nopaging'    => true,
  'order'       => 'ASC',
  'orderby'    => 'menu_order'
));

if ( $projects->have_posts() ) : while ( $projects->have_posts() ) : $projects->the_post(); 
    $images = get_field('images'); 

    if ($images): // Only load projects with images to the grid! Not just a featured image.

        if ( '' != get_the_post_thumbnail() ){
            $img_id =  get_post_thumbnail_id($post->ID);
            $featured_image_obj =  wp_get_attachment_image_src( $img_id, 'project-grid');
            $featured_image = $featured_image_obj[0];
        } 

   // endif; 
   
   
?>

  <div class="project" id="project-<?php the_ID(); ?>" style="background-image: url(<?php echo $featured_image ?>);">
   
      <a href="<?php the_permalink()?>" title="<?php the_title();?>"></a>
    
  </div>
<?php endif; endwhile; endif; wp_reset_postdata();?>
