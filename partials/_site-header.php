<div class="wrapper">
  <div class="mobile-nav-toggle"></div>
  <div class="mobile-nav-wrapper">
      <?php wp_nav_menu( array( 'container' => 'nav', 'container_class' => 'nav mobile-nav', 'menu' => 'Header Menu' ) ); ?>
  </div>
  <a href="/" id="logo">
    <!-- <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo.png" alt=""> -->
  </a>
  
  <?php wp_nav_menu( array( 'container' => 'nav', 'container_class' => 'nav primary-nav', 'menu' => 'Header Menu' ) ); ?>
</div>  