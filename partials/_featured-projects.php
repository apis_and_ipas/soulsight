<?php

$featured_project = new WP_Query(array(
  'post_type' => 'project',
  'post_status' => 'publish',
  'meta_query' => array(
       array(
           'key' => 'featured_project',
           'value' => '1'
       )
   )
));

if ( $featured_project->have_posts() ) : while ( $featured_project->have_posts() ) : $featured_project->the_post(); ?>
  <div class="featured-project" id="project-<?php the_ID(); ?>">
    <div class="flipper">
      <a href="<?php the_permalink()?>" title="<?php the_title();?>">
        <div class="front">
          
            <?php 
              // if (get_field('featured_image') ):
              //     $src = get_field('featured_image');
                  $title = get_the_title();
              //     echo "<img src='{$src}' title='{$title}'/>";
              // else:
                // the_post_thumbnail();
                $attachment_id = get_post_thumbnail_id( get_the_ID() );
                $src = wp_get_attachment_image_src( $attachment_id, 'project-featured'); 
                echo "<img src='{$src[0]}' title='{$title}'/>";
              // endif; 
            ?>
        </div>
        <div class="back">
            View More
        </div>
      </a>
    </div>
    
  </div>
<?php endwhile; endif; wp_reset_postdata();?>
