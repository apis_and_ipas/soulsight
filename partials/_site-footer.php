<!-- Footer Partial -->
<div class="wrapper">
  <div class="grid-3">
    <div class="head">Where To Find Us</div>
    <a href="https://www.google.com/maps/place/1899+2nd+St/@42.1870234,-87.8016627,17z/data=!3m1!4b1!4m2!3m1!1s0x880fc1bb6861b139:0x1c03f52122bf90bf" target="_blank">
      1899 Second Street<br/>
      Highland Park, IL 60035
    </a>
  </div>
  <div class="grid-3">
    <div class="head">Get In Touch</div>
    <a href="mailto:info@soulsight.com">info@soulsight.com</a><br/>
    <a href="tel:847-681-4444">847-681-4444</a>
  </div>
  <div class="grid-2">&nbsp;</div>
  <div class="grid-4">
    <ul class="social-icons">
      <li>
        <a href="<?php echo esc_attr( get_field( 'facebook','options' ) ); ?>" class="social fb" target="_blank"></a>
        <a href="<?php echo esc_attr( get_field( 'twitter','options' ) ); ?>" class="social twt" target="_blank"></a>
        <a href="<?php echo esc_attr( get_field( 'pinterest','options' ) ); ?>" class="social ins" target="_blank" target="_blank"></a>
        <a href="<?php echo esc_attr( get_field( 'linkedin','options' ) ); ?>" class="social lnkd" target="_blank"></a>
        <a href="<?php bloginfo( 'rss2_url' );?> " class="social rss" target="_blank"></a>
      </li>
    </ul>
    <div class="copyright">&copy; Soulsight <?php echo ss_copyright_range(); ?></div>
    <!-- <div class="doejo">
      made by <a href="http://doejo.com/"></a>
    </div> -->
  </div>
</div>
<div class='footer-map'>
  
</div>