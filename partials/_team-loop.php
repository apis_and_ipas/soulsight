<?php

$team_query  = new WP_Query(array(
  'post_type'   => 'team-member',
  'post_status' => 'publish',
  'nopaging'    => true,
  'order'       => 'ASC',
  'orderby'    => 'menu_order'
));


if ( $team_query->have_posts() ) : while ( $team_query->have_posts() ) : $team_query->the_post(); 
    $image_url = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'team-grid');
?>
  <div class="team-member" id="member-<?php the_ID(); ?>" ontouchstart="this.classList.toggle('hover');">
    <div class="flipper">
      <div class="front" style="background: url(<?php echo esc_attr( $image_url[0] ); ?>) 0 0 no-repeat;">
      </div>
      <div class="back">
          <div class="member-name"><?php the_title();?></div>
          <div class="member-title"><?php the_field('job_title');?></div>
      </div>
    </div>
  </div>
<?php endwhile; endif; wp_reset_postdata();?>
