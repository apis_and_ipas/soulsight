<?php

$clients = new WP_Query(array(
  'post_type'   => 'client',
  'post_status' => 'publish',
  'nopaging'    => true,
  'order'       => 'ASC'
));

if ( $clients->have_posts() ) : while ( $clients->have_posts() ) : $clients->the_post(); ?>

  <div class="client" id="client-<?php the_ID(); ?>">
   
      <img src="<?php the_field('logo')?>" alt="<?php the_title(); ?>">
    
  </div>

<?php endwhile; endif; wp_reset_postdata();?>
